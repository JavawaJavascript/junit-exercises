package math;

/**
 * <p>Title: Main - driver class for Unit Testing assignment</p>
 * <p>Description: assignment on Unit testing using JUnit 
 * </p>
 * 
 * @author jacob
 * @email jacob.hendricks@uwrf.edu
 * @version 1
 *
 */

public class Main {

	public static void main(String[] args) {
		FloatComputations floatComp = new FloatComputations( 1.0f, 0.9f );
		System.out.println( "FloatComputations object floatComp has: " + floatComp );
		
		System.out.println("  Subtraction computation gives us: " + floatComp.subtract() );
		System.out.println("  Division    computation gives us: " + floatComp.divide() );
		
		
		IntComputations intComp = new IntComputations( 12, 3 );
		System.out.println( "IntComputations object intComp has: " + intComp );
		
		System.out.println("  Subtraction computation gives us: " + intComp.subtract() );
		System.out.println("  Division    computation gives us: " + intComp.divide() );
		
	}

}
