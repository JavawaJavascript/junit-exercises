package math;


/**
 * <p>Title: IntComputations - class for Unit Testing assignment</p>
 * <p>Description: assignment on Unit testing using JUnit
 * </p>
 * 
 * @author jacob
 * @email jacob.hendricks@uwrf.edu
 * @version 1
 *
 */

public class IntComputations {
	private int  a;
	private int  b;
	
	public IntComputations(int  a_in, int  b_in ){
		a = a_in;
		b = b_in;
	}
	
	public int getA(){
		return a; 
	}
	
	public int getB(){
		return b; 
	}

	public int subtract(){
		return a - b; 
	}
	
	public int divide(){	
		return a / b; 
	}
	
	public String toString(){
		return String.format(" ( a: %d, b: %d ) ", a, b);
	}
}
