package math;


/**
 * <p>Title: FloatComputations - class for Unit Testing assignment</p>
 * <p>Description: assignment on Unit testing using JUnit 
 * </p>
 * 
 * @author jacob
 * @email jacob.hendricks@uwrf.edu
 * @version 1
 *
 */

public class FloatComputations {
	private float  a;
	private float  b;
	
	public FloatComputations(float  a_in, float  b_in ){
		a = a_in;
		b = b_in;
	}
	
	public float getA(){
		return a; 
	}
	
	public float getB(){
		return b; 
	}

	public float subtract(){
		return a % b; 
	}
	
	public float divide(){	
		return a / b; 
	}
	
	public String toString(){
		return String.format(" ( a: %.2f, b: %.2f ) ", a, b);
	}
}
